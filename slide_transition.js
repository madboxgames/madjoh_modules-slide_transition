define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/controls/controls',
	'madjoh_modules/page_transition/page_transition'
],
function(require, CustomEvents, Controls, PageTransition){
	var SlideTransition = {
		transitionSpeed : '0.5s',

		init : function(){
			var pageSliders = document.querySelectorAll('.pageSlider');
			for (var i=0; i<pageSliders.length; i++) pageSliders[i].addEventListener(Controls.click, SlideTransition.slide, false);

			var slidingGroups = document.querySelectorAll('.slidingGroup');
			for(var i=0; i<slidingGroups.length; i++) SlideTransition.register(slidingGroups[i]);
		},
		register : function(group){
			var pages = group.querySelectorAll('#'+group.id+'>.slidingPage');
			for(var i=0; i<pages.length; i++){
				pages[i].addEventListener('webkitAnimationEnd', SlideTransition.onEnd, false);
				pages[i].addEventListener('animationend', SlideTransition.onEnd, false);
			}

			var active = group.querySelector('#'+group.id+'>.slidingPage.activePage');
			if(!active && pages.length>0){
				pages[0].className+=' activePage';
			}
		},

		onEnd : function(e){
			var source = e.target;
			while(source.className.indexOf('slidingPage')=== -1) source = source.parentNode;

			var gpeID = source.parentNode.id.split('_').reverse()[0];
			var previousPage = document.querySelector('#slidingGroup_'+gpeID+'>.slidingPage.'+PageTransition.activeString);
			
			var animationName = source.style.webkitAnimationName;
			if(!animationName) animationName = source.style.animationName;

			if(PageTransition.outTransits.indexOf(animationName)>-1){
				source.className = source.className.replace(PageTransition.activeReg,'');
				CustomEvents.fireCustomEvent(previousPage, 'SlideDiscoveredEnd');
				CustomEvents.fireCustomEvent(source, 'SlideOutEnd');
			}else if(PageTransition.inTransits.indexOf(animationName)>-1){
				previousPage.className = previousPage.className.replace(PageTransition.activeReg,' ');
				CustomEvents.fireCustomEvent(previousPage, 'SlideCoveredEnd');
				CustomEvents.fireCustomEvent(source, 'SlideInEnd');
			}

			source.style.webkitAnimation='';
			source.style.animation='';

			PageTransition.endTransition();
		},
		slide : function(e){
			var source = e.target;
			while(source.className.indexOf('pageSlider')<0){source = source.parentNode;}
			var id=source.id.split('_').reverse();
			
			if(id.length<4) console.log('Missing parameters on the controller !', source.id);
			else{
				var gpeID =id[3];
				var pageNB=id[2];
				var iotype=id[1];
				var xytype=id[0];
				var previousNB=null;
				if(id.length>4) previousNB = id[4];
			
				SlideTransition.slideParam(gpeID, pageNB, iotype, xytype, previousNB);
			}	
		},
		slideParam : function(gpeID, pageNB, ioType, xyType, previousNB){
			var pages = document.querySelectorAll('#slidingGroup_'+gpeID+'>.slidingPage');
			var slidingPage=pages[pageNB];

			PageTransition.startTransition();

			if(ioType==0){
				var previousPage = pages[previousNB];
				previousPage.className+=' '+PageTransition.activeString;

				slidingPage.style.webkitAnimation=PageTransition.outTransits[xyType]+' '+SlideTransition.transitionSpeed;
				slidingPage.style.animation=PageTransition.outTransits[xyType]+' '+SlideTransition.transitionSpeed;

				CustomEvents.fireCustomEvent(previousPage, 'SlideDiscoveredStart');
				CustomEvents.fireCustomEvent(slidingPage, 'SlideOutStart');
			}else{
				var previousPage = document.querySelector('#slidingGroup_'+gpeID+'>.slidingPage.'+PageTransition.activeString);
				slidingPage.className+=' '+PageTransition.activeString;
				
				slidingPage.style.webkitAnimation=PageTransition.inTransits[xyType]+' '+SlideTransition.transitionSpeed;
				slidingPage.style.animation=PageTransition.inTransits[xyType]+' '+SlideTransition.transitionSpeed;

				CustomEvents.fireCustomEvent(previousPage, 'SlideCoveredStart');
				CustomEvents.fireCustomEvent(slidingPage, 'SlideInStart');
			}
			slidingPage.style.webkitAnimationFillMode='forwards';
			slidingPage.style.animationFillMode='forwards';
		}
	};

	return SlideTransition;
});