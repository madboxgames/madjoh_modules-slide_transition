# slide_transition #

v1.0.3

This module enables pages transition with a slide animation (a page that comes in covers the actual).

## Gettings Started ##

### Defining the HTML properly ###

** For this code to work ** you need to :

- Define the ** Sliding Groups **:
	- class = 'slidingGroup'
	- id = 'slidingGroup_X' where :
		- X is the group id.

- Define the ** Sliding Pages **:
	- class = 'slidingPage transitPage [activePage]'
	** /!\ There can only be one activePage in a Group /!\ **
	N.B. If no active page is defined, the first one will be the active page by default.

- Define the ** Controllers **:
	- Slide to a ** specific page ** :
		- class = 'pageSlider'
		- id = 'something_W_X_Y_A_B where :
			- X is the *group id*.
			- Y is the *target page number* ** /!\ starting at 0 /!\ **
			- A is the type of mouvement : *in/out*. (1 or 0)
			- B is the type of mouvement : *horizontal / vertical*. (0 or 1)
			- W is mandatory only for out transition. It is the *previous page* number.	

- ** Further Features ** :
	- You can change the transition speed by overriding SlideTransition.transitionSpeed *(String)*. Default value is '0.5s'.

## Example ##
```html
<!DOCTYPE html>
<html>
	<head>
		<head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, width=device-width, height=device-height, minimal-ui"/>
        
        <!-- MADJOH STYLES -->
            <link rel="stylesheet" type="text/css" href="MadJohModules/Styling_1.0.0/Styling_1.0.0.css" id="Styling_1.0.0_css"/>
            <link rel="stylesheet" type="text/css" href="MadJohModules/PageTransition_1.1.0/PageTransition_1.1.0.css" id="PageTransition_1.1.0_css"/>
        <!-- /MADJOH STYLES -->

        <!-- APP STYLES -->
            <link rel="stylesheet" type="text/css" href="app/styles/main.css" />
        <!-- /APP STYLES -->

        <title>Welcome</title>
    </head>
	<body>
		<div class="page-container">
			<div id="p_body">
				<div class="Page">
					<!-- SWEEPING PAGES -->
					<div class="fullTopPage slidingGroup" id="slidingGroup_1">
						<div class="slidingPage transitPage">
							<p>First Page</p>
							<ul>
								<li class="pageSlider" id="slider_1_1_1_0">Next</li>
							</ul>
						</div>
						<div class="slidingPage transitPage">
							<p>Second Page</p>
							<ul>
								<li class="pageSlider" id="slider_1_2_1_1">Next</li>
								<li class="pageSlider" id="slider_0_1_1_0_0">Previous</li>
							</ul>
						</div>
						<div class="slidingPage transitPage">
							<p>Third Page</p>
							<ul>
								<li class="pageSlider" id="slider_1_1_2_0_1">Previous</li>
							</ul>
						</div>
					</div>
				</div>

				<!-- CACHE PAGE -->
                <div id="cacheBlock"></div>
			</div>
		</div>

		<div id="pub">
			<section>PUB1</section>
			<section>PUB2</section>
			<section>PUB3</section>
		</div>

		<script type="text/javascript" src="MadJohModules/MadJohRequire_1.0.0/MadJohRequire_1.0.0.js"></script>
		<script data-main="app" type="text/javascript" src="lib/require.js"></script>
	</body>
</html>
```

### Controlling the transition with  JavaScript ###

You can control the transition as follows :

```js
SlideTransition.slideParam(gpeID, pageNB, iotype, xytype, [previousNB]);
```

You can register a new slidingGroup (for HTML generated after the page has been loaded for example) as follows :

```js
var group = document.getElementById('gpeID');
SlideTransition.register(group);
```